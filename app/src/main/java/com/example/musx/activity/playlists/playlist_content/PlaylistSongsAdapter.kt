package com.example.musx.activity.playlists.playlist_content

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.musx.R
import com.example.musx.activity.playlists.playlist_content.dialog.EditSongFromPlaylistDialog
import com.example.musx.activity.playlists.playlist_content.saved_song.SavedSongActivity
import com.example.musx.model.Playlist
import com.example.musx.model.SavedSong
import com.example.musx.util.SaveSongsUtil


class PlaylistSongsAdapter(
    val playlist: Playlist,
    private val mCtx: PlaylistSongsActivity
) :
    RecyclerView.Adapter<PlaylistSongsAdapter.SongHolder>() {

    companion object {
        const val FIND_MUSIC = 1337
        const val SONG_DATA_KEY =
            "com.example.musx.activity.playlists.playlist_content.SONG_DATA_KEY"
        const val PLAYLIST_KEY =
            "com.example.musx.activity.playlists.playlist_content.PLAYLIST_KEY"
    }

    var selectedSong: SavedSong? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.playlist_song_layout, parent, false)
        return SongHolder(v)
    }

    override fun getItemCount(): Int = playlist.songs.size

    override fun onBindViewHolder(holder: SongHolder, position: Int) {
        val song: SavedSong = playlist.songs[position]
        song.title.let { holder.setTitle(it) }
        song.artist.let { holder.setArtist(it) }

        holder.mSongOptionsTextView.setOnClickListener {
            val popup = PopupMenu(mCtx, holder.mSongOptionsTextView)
            popup.inflate(R.menu.song_menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.mi_rename_playlist_song -> {
                        EditSongFromPlaylistDialog(mCtx, song, playlist).show()
                    }
                    R.id.mi_attach_song_form_device -> {
                        // TODO add check for storage permissions!
                        selectedSong = song
                        val intent = Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                        )
                        mCtx.startActivityForResult(
                            Intent.createChooser(
                                intent,
                                "Select music file"
                            ),
                            FIND_MUSIC
                        )
                    }
                    R.id.mi_remove_from_playlist -> {
                        val updatedPlaylist =
                            SaveSongsUtil.removeSavedSongFromPlaylist(playlist, song)
                        mCtx.updateData(updatedPlaylist)
                    }
                }
                false
            }
            popup.show()
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mCtx, SavedSongActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putSerializable(SONG_DATA_KEY, song)
                bundle.putSerializable(PLAYLIST_KEY, playlist)
                putExtras(bundle)
            }

            mCtx.startActivity(intent)
        }
    }

    class SongHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {
            const val MAX_TITLE_LENGTH = 30
            const val MAX_ARTIST_LENGTH = 50
        }

        // Playlist name
        private val mTitleTextView: TextView = itemView.findViewById(R.id.tv_song_title)

        // Playlist description
        private val mArtistTextView: TextView =
            itemView.findViewById(R.id.tv_song_artist)

        // Playlist options button
        val mSongOptionsTextView: TextView =
            itemView.findViewById(R.id.tv_song_options)

        fun setTitle(title: String) {
            mTitleTextView.text = if (title.length <= MAX_TITLE_LENGTH) {
                title
            } else {
                title.substring(0, MAX_TITLE_LENGTH - 2) + ".."
            }
        }

        fun setArtist(artist: String) {
            mArtistTextView.text = if (artist.length <= MAX_ARTIST_LENGTH) {
                artist
            } else {
                artist.substring(0, MAX_ARTIST_LENGTH - 2) + ".."
            }
        }
    }
}