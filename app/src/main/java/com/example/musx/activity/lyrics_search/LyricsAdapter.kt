package com.example.musx.activity.lyrics_search

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.musx.R
import com.example.musx.activity.lyrics_search.lyrics_content.LyricsContentActivity
import com.example.musx.model.SavedSong
import com.example.musx.spi.audd.model.responsemodel.SongData
import com.example.musx.util.SaveSongsUtil
import com.google.android.material.snackbar.Snackbar


class LyricsAdapter(
    private val songs: MutableList<SongData>,
    private val mCtx: Context,
    private val mRoot: ConstraintLayout
) :
    RecyclerView.Adapter<LyricsAdapter.SongHolder>() {


    companion object {
        const val SONG_DATA_KEY = "com.example.musx.activity.lyrics_search.SONG_DATA_KEY"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.lyrics_layout, parent, false)
        return SongHolder(v)
    }

    override fun getItemCount(): Int = songs.size

    override fun onBindViewHolder(holder: SongHolder, position: Int) {
        val song: SongData = songs[position]
        song.title.let { holder.setTitle(it) }
        song.artist.let { holder.setArtist(it) }

        holder.mLyricsOptionsTextView.setOnClickListener {
            val popup = PopupMenu(mCtx, holder.mLyricsOptionsTextView)
            popup.inflate(R.menu.lyrics_menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.mi_add_to_playlist -> {
                        val b: AlertDialog.Builder = AlertDialog.Builder(mCtx)
                        b.setTitle(R.string.action_add_song_to_a_playlist)
                        val playlists = SaveSongsUtil.getSavedPlaylists().playlists

                        if (playlists.isEmpty()) {
                            Snackbar.make(mRoot, "Create playlist first..", Snackbar.LENGTH_LONG)
                                .show()
                            return@setOnMenuItemClickListener false
                        }

                        val playlistNames: List<String> = playlists.map { s -> s.name }

                        b.setItems(
                            playlistNames.toTypedArray()
                        ) { dialog, which ->
                            try {
                                SaveSongsUtil.addSongToPlaylist(
                                    playlists[which],
                                    SavedSong(song.artist, song.title, song.lyrics)
                                )
                                Toast.makeText(
                                    mCtx,
                                    "Successfully added song to ${playlists[which].name}",
                                    Toast.LENGTH_LONG
                                ).show()
                            } catch (e: IllegalArgumentException) {
                                Toast.makeText(
                                    mCtx,
                                    e.message,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                            dialog.dismiss()
                        }
                        b.show()
                    }
                }
                false
            }
            popup.show()
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mCtx, LyricsContentActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putSerializable(SONG_DATA_KEY, songs[position])
                putExtras(bundle)
            }

            mCtx.startActivity(intent)
        }
    }

    class SongHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {
            const val MAX_TITLE_LENGTH = 30
            const val MAX_ARTIST_LENGTH = 50
        }

        // Playlist name
        private val mTitleTextView: TextView = itemView.findViewById(R.id.tv_lyrics_title)

        // Playlist description
        private val mArtistTextView: TextView =
            itemView.findViewById(R.id.tv_lyrics_artist)

        // Playlist options button
        val mLyricsOptionsTextView: TextView =
            itemView.findViewById(R.id.tv_lyrics_options)

        fun setTitle(title: String) {
            mTitleTextView.text = if (title.length <= MAX_TITLE_LENGTH) {
                title
            } else {
                title.substring(0, MAX_TITLE_LENGTH - 2) + ".."
            }
        }

        fun setArtist(artist: String) {
            mArtistTextView.text = if (artist.length <= MAX_ARTIST_LENGTH) {
                artist
            } else {
                artist.substring(0, MAX_ARTIST_LENGTH - 2) + ".."
            }
        }
    }
}