package com.example.musx.activity.lyrics_search.lyrics_content

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.musx.R
import com.example.musx.activity.lyrics_search.LyricsAdapter
import com.example.musx.model.SavedSong
import com.example.musx.spi.audd.model.responsemodel.SongData
import com.example.musx.util.SaveSongsUtil
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

class LyricsContentActivity : AppCompatActivity() {

    // Views
    private lateinit var mSongTitleTextView: TextView
    private lateinit var mSongAuthorTextView: TextView
    private lateinit var mSongLyricsTextView: TextView

    private lateinit var mShareLyricsActionButton: FloatingActionButton

    private lateinit var mRoot: ConstraintLayout

    // Model
    private lateinit var songData: SongData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lyrics_content)

        mSongTitleTextView = findViewById(R.id.tv_lyrics_content_song_title)
        mSongAuthorTextView = findViewById(R.id.tv_lyrics_content_song_author)
        mSongLyricsTextView = findViewById(R.id.tv_lyrics_content_song_lyrics)

        mRoot = findViewById(R.id.cl_lyrics_content_root)

        initFloatingButton()

        loadLyrics()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.lyrics_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.mi_add_to_playlist -> {
            val b: AlertDialog.Builder = AlertDialog.Builder(this)
            b.setTitle(R.string.action_add_song_to_a_playlist)
            val playlists = SaveSongsUtil.getSavedPlaylists().playlists

            if (playlists.isEmpty()) {
                Snackbar.make(mRoot, "Create playlist first..", Snackbar.LENGTH_LONG)
                    .show()
                false
            } else {
                val playlistNames: List<String> = playlists.map { s -> s.name }

                b.setItems(
                    playlistNames.toTypedArray()
                ) { dialog, which ->
                    try {
                        SaveSongsUtil.addSongToPlaylist(
                            playlists[which],
                            SavedSong(songData.artist, songData.title, songData.lyrics)
                        )
                        Toast.makeText(
                            this,
                            "Successfully added song to ${playlists[which].name}",
                            Toast.LENGTH_LONG
                        ).show()
                    } catch (e: IllegalArgumentException) {
                        Toast.makeText(
                            this,
                            e.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    dialog.dismiss()
                }
                b.show()
                true
            }
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun loadLyrics() {
        val possibleSongData = intent.extras?.getSerializable(LyricsAdapter.SONG_DATA_KEY)
        if (possibleSongData is SongData) {
            songData = possibleSongData
            mSongTitleTextView.text = songData.title
            mSongAuthorTextView.text = songData.artist
            mSongLyricsTextView.text = songData.lyrics
        }
    }

    private fun initFloatingButton() {
        mShareLyricsActionButton = findViewById(R.id.fab_share_lyrics)

        mShareLyricsActionButton.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, songData.lyrics)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }
    }
}
