package com.example.musx.activity.playlists.dialog

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.musx.R
import com.example.musx.activity.playlists.PlaylistsActivity
import com.example.musx.activity.playlists.playlist_content.PlaylistSongsActivity
import com.example.musx.model.Playlist
import com.example.musx.util.SaveSongsUtil
import com.google.android.material.snackbar.Snackbar

open class RenamePlaylistDialog(
    context: Context,
    private val playlist: Playlist
) : Dialog(context),
    View.OnClickListener {

    private val activity = context as Activity

    private lateinit var mPlaylistNameEditText: EditText
    private lateinit var mRenamePlaylistButton: Button

    private lateinit var mRoot: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.rename_playlist_dialog_layout)

        this.window?.setLayout(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT
        )

        mPlaylistNameEditText = findViewById(R.id.et_rename_playlist_title)
        mRenamePlaylistButton = findViewById(R.id.b_rename_playlist)
        mRenamePlaylistButton.setOnClickListener(this)

        mRoot = findViewById(R.id.cl_rename_playlist_dialog_root)
    }

    override fun onClick(v: View?) {
        val newTitle = mPlaylistNameEditText.text.toString()

        if (newTitle.isBlank()) {
            Snackbar.make(mRoot, "Title have to be filled", Snackbar.LENGTH_LONG)
                .show()
            return
        }

        try {
            SaveSongsUtil.editPlaylistName(playlist, newTitle)

            playlist.name = newTitle

            activity.let {
                when (it) {
                    is PlaylistSongsActivity -> it.updateData(playlist)
                    is PlaylistsActivity -> it.updateData(
                        SaveSongsUtil.getSavedPlaylists().playlists
                    )
                    // TODO properly handle this error
                    else -> throw IllegalArgumentException("Unexpected error has occurred!")
                }
            }
            this.dismiss()
        } catch (e: IllegalArgumentException) {
            val message = e.message
            if (message != null) {
                Snackbar.make(mRoot, message, Snackbar.LENGTH_LONG)
                    .show()
            } else {
                Snackbar.make(
                        mRoot,
                        "Such a playlist already exists!",
                        Snackbar.LENGTH_LONG
                    )
                    .show()
            }
        }
    }
}