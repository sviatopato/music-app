package com.example.musx.activity.playlists

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.musx.R
import com.example.musx.activity.playlists.dialog.EditPlaylistDescriptionDialog
import com.example.musx.activity.playlists.dialog.RenamePlaylistDialog
import com.example.musx.activity.playlists.playlist_content.PlaylistSongsActivity
import com.example.musx.model.Playlist
import com.example.musx.util.SaveSongsUtil


class PlaylistAdapter(private val playlists: MutableList<Playlist>, private val mCtx: Context) :
    RecyclerView.Adapter<PlaylistAdapter.PlaylistHolder>() {

    companion object {
        const val PLAYLIST_KEY = "com.example.musx.activity.playlist.PLAYLIST_KEY"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.playlist_layout, parent, false)
        return PlaylistHolder(v)
    }

    override fun getItemCount(): Int = playlists.size

    override fun onBindViewHolder(holder: PlaylistHolder, position: Int) {
        val playlist: Playlist = playlists[position]
        holder.setPlaylistName(playlist.name)
        holder.setPlaylistDescription(playlist.description)

        holder.mPlaylistOptionsTextView.setOnClickListener {
            val popup = PopupMenu(mCtx, holder.mPlaylistOptionsTextView)
            popup.inflate(R.menu.playlist_menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.mi_rename_playlist -> {
                        RenamePlaylistDialog(mCtx, playlist).show()
                    }
                    R.id.mi_edit_playlist_description -> {
                        EditPlaylistDescriptionDialog(mCtx, playlist).show()
                    }
                    R.id.mi_delete_playlist -> {
                        SaveSongsUtil.removePlaylist(playlist)
                        if (mCtx is PlaylistsActivity) {
                            mCtx.updateData(SaveSongsUtil.getSavedPlaylists().playlists)
                        }
                    }
                }
                true
            }
            popup.show()
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mCtx, PlaylistSongsActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putSerializable(PLAYLIST_KEY, playlist)
                putExtras(bundle)
            }

            mCtx.startActivity(intent)
        }
    }

    class PlaylistHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        companion object {
            const val MAX_NAME_LENGTH = 25
            const val MAX_DESCRIPTION_LENGTH = 100
        }

        // Playlist name
        private val mPlaylistNameTextView: TextView = itemView.findViewById(R.id.tv_playlist_name)

        // Playlist description
        private val mPlaylistDescriptionTextView: TextView =
            itemView.findViewById(R.id.tv_playlist_description)

        // Playlist options button
        val mPlaylistOptionsTextView: TextView =
            itemView.findViewById(R.id.tv_playlist_options)

        fun setPlaylistName(name: String) {
            mPlaylistNameTextView.text = if (name.length <= MAX_NAME_LENGTH) {
                name
            } else {
                name.substring(0, MAX_NAME_LENGTH - 2) + ".."
            }
        }

        fun setPlaylistDescription(description: String) {
            mPlaylistDescriptionTextView.text = if (description.length <= MAX_DESCRIPTION_LENGTH) {
                description
            } else {
                description.substring(0, MAX_DESCRIPTION_LENGTH - 2) + ".."
            }
        }

        fun getPlaylistName(): String {
            return mPlaylistNameTextView.text.toString()
        }

        fun getPlaylistDescription(): String {
            return mPlaylistDescriptionTextView.text.toString()
        }
    }
}