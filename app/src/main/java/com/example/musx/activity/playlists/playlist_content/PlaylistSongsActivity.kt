package com.example.musx.activity.playlists.playlist_content

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.musx.R
import com.example.musx.activity.playlists.PlaylistAdapter
import com.example.musx.activity.playlists.dialog.EditPlaylistDescriptionDialog
import com.example.musx.activity.playlists.dialog.RenamePlaylistDialog
import com.example.musx.model.Playlist
import com.example.musx.model.SavedSong
import com.example.musx.util.FileUtils
import com.example.musx.util.SaveSongsUtil


class PlaylistSongsActivity : AppCompatActivity() {

    companion object {
        const val PLAYLIST_RESTORE_KEY =
            "com.example.musx.activity.playlists.playlist_content.PLAYLIST_RESTORE_KEY"
    }

    // Activity members
    // Songs RecyclerView
    private lateinit var mSongsRecyclerView: RecyclerView
    private lateinit var songsAdapter: PlaylistSongsAdapter

    private lateinit var mNoSongsTextView: TextView

    private lateinit var mPlaylistNameTextView: TextView

    // Model
    private lateinit var playlist: Playlist

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_content)

        if (intent.hasExtra(PlaylistAdapter.PLAYLIST_KEY)) {
            playlist = intent.getSerializableExtra(PlaylistAdapter.PLAYLIST_KEY) as Playlist
        } else if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState)
        }

        mPlaylistNameTextView = findViewById(R.id.tv_playlist_songs_name)

        mSongsRecyclerView = findViewById(R.id.rv_playlist_songs)
        mSongsRecyclerView.setHasFixedSize(true)
        mSongsRecyclerView.layoutManager = LinearLayoutManager(this)

        mNoSongsTextView = findViewById(R.id.tv_no_songs)

        loadRecyclerViewItems()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putSerializable(PLAYLIST_RESTORE_KEY, playlist)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        playlist = savedInstanceState.getSerializable(PLAYLIST_RESTORE_KEY) as Playlist
        updateData(playlist)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.playlist_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.mi_rename_playlist -> {
            RenamePlaylistDialog(this, playlist).show()
            true
        }
        R.id.mi_edit_playlist_description -> {
            EditPlaylistDescriptionDialog(this, playlist).show()
            true
        }
        R.id.mi_delete_playlist -> {
            SaveSongsUtil.removePlaylist(playlist)
            finish()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PlaylistSongsAdapter.FIND_MUSIC -> if (resultCode == Activity.RESULT_OK) {
                val uri: Uri? = data?.data
                val song: SavedSong? = songsAdapter.selectedSong
                val playlist: Playlist = songsAdapter.playlist

                uri?.let { songUri ->
                    song?.let { s ->
                        updateData(
                            SaveSongsUtil.attachSongToSavedSong(
                                playlist, s, FileUtils.getPath(applicationContext, songUri)
                            )
                        )
                        Toast.makeText(this, "Successfully attached song", Toast.LENGTH_LONG).show()
                        return
                    }
                }
                Toast.makeText(this, "Failed to attach the song", Toast.LENGTH_LONG).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun loadRecyclerViewItems() {
        songsAdapter = PlaylistSongsAdapter(playlist, this)
        mSongsRecyclerView.adapter = songsAdapter

        updateData(playlist)
    }

    fun updateData(playlist: Playlist) {
        mPlaylistNameTextView.text = playlist.name

        if (playlist.songs.isEmpty()) {
            mNoSongsTextView.visibility = TextView.VISIBLE
            mSongsRecyclerView.visibility = RecyclerView.GONE
        } else {
            mNoSongsTextView.visibility = TextView.GONE
            mSongsRecyclerView.visibility = RecyclerView.VISIBLE
        }

        if (playlist === this.playlist) {
            songsAdapter.notifyDataSetChanged()
        } else {
            this.playlist.songs.clear()
            this.playlist.songs.addAll(playlist.songs)
            songsAdapter.notifyDataSetChanged()
        }
    }
}
