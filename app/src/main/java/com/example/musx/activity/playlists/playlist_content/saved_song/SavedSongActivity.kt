package com.example.musx.activity.playlists.playlist_content.saved_song

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.musx.R
import com.example.musx.activity.playlists.playlist_content.PlaylistSongsAdapter
import com.example.musx.model.Playlist
import com.example.musx.model.SavedSong
import kotlinx.android.synthetic.main.activity_saved_song.*
import java.io.File
import java.io.FileInputStream
import java.util.concurrent.TimeUnit

class SavedSongActivity : AppCompatActivity(), View.OnClickListener {

    private var mediaPlayer: MediaPlayer? = null

    private lateinit var playlist: Playlist
    private lateinit var song: SavedSong

    var currPosition = 0

    private var handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            val currentPos = msg.what

            mSongProgressSeekBar.progress = currentPos
            currPosition = currentPos

            mTimeElapsedTextView.text = String.format(
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(currentPos.toLong()),
                TimeUnit.MILLISECONDS.toSeconds(currentPos.toLong()) -
                        TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(
                                currentPos.toLong()
                            )
                        )
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saved_song)

        playlist = intent.getSerializableExtra(PlaylistSongsAdapter.PLAYLIST_KEY) as Playlist
        song = intent.getSerializableExtra(PlaylistSongsAdapter.SONG_DATA_KEY) as SavedSong

        mPlayButton.setOnClickListener(this)

        updateData(song, 0)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.song_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.mi_rename_playlist_song -> {
            TODO("Not implemented for demo purposes")
        }
        R.id.mi_attach_song_form_device -> {
            TODO("Not implemented for demo purposes")
        }
        R.id.mi_remove_from_playlist -> {
            TODO("Not implemented for demo purposes")
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun updateData(newSong: SavedSong, position: Long) {
        song = newSong
        mSongTitleTextView.text = song.title
        mAuthorTextView.text = song.artist
        mLyricsTextView.text = song.lyrics

        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            if (song.hasSongFromFileAttached()) {
                mSongPlayerRoot.visibility = View.VISIBLE

                mPlayButton.setBackgroundResource(R.drawable.baseline_play_arrow_24)
                if (mediaPlayer == null) {
                    mediaPlayer = MediaPlayer()
                    mediaPlayer?.let {
                        it.isLooping = true

                        it.setAudioAttributes(
                            AudioAttributes.Builder()
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build()
                        )

                        it.setDataSource(
                            FileInputStream(File(Uri.parse(song.attachedSongPath).path.toString())).fd
                        )

                        it.prepare()

                        mTotalTimeTextView.text = String.format(
                            "%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(it.duration.toLong()),
                            TimeUnit.MILLISECONDS.toSeconds(it.duration.toLong()) -
                                    TimeUnit.MINUTES.toSeconds(
                                        TimeUnit.MILLISECONDS.toMinutes(
                                            it.duration.toLong()
                                        )
                                    )
                        )

                        mSongProgressSeekBar.max = it.duration
                        mSongProgressSeekBar.setOnSeekBarChangeListener(object :
                            SeekBar.OnSeekBarChangeListener {
                            override fun onProgressChanged(
                                seekBar: SeekBar?,
                                progress: Int,
                                fromUser: Boolean
                            ) {
                                if (fromUser) {
                                    mediaPlayer?.seekTo(progress)
                                }
                            }

                            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                            }

                            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                            }
                        })

                        Thread {
                            while (mediaPlayer != null) {
                                try {
                                    val message = Message()
                                    message.what = mediaPlayer?.currentPosition!!
                                    handler.sendMessage(message)
                                    Thread.sleep(1000)
                                } catch (e: Exception) {
                                    // TODO log somewhere
                                }
                            }
                        }.start()
                    }
                }

                mediaPlayer?.seekTo(position.toInt())
            } else {
                mSongPlayerRoot.visibility = View.GONE
            }
        } else {
            mSongPlayerRoot.visibility = View.GONE
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                1
            )
        }
    }

    override fun onResume() {
        super.onResume()
        updateData(song, currPosition.toLong())
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer?.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer?.release()
        mediaPlayer = null
    }

    override fun onClick(v: View?) {
        mediaPlayer?.let {
            if (it.isPlaying) {
                // pause
                mPlayButton.setBackgroundResource(R.drawable.baseline_play_arrow_24)
                it.pause()
            } else {
                // stop
                mPlayButton.setBackgroundResource(R.drawable.baseline_stop_24)
                it.start()
            }
        }
    }
}
