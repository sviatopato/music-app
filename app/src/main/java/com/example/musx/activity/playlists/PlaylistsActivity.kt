package com.example.musx.activity.playlists

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.musx.R
import com.example.musx.activity.playlists.dialog.CreatePlaylistDialog
import com.example.musx.model.Playlist
import com.example.musx.receiver.NotificationReceiver
import com.example.musx.util.NotificationUtil
import com.example.musx.util.SaveSongsUtil
import com.google.android.material.floatingactionbutton.FloatingActionButton


class PlaylistsActivity : AppCompatActivity() {

    // Activity members
    // Playlist RecyclerView
    private lateinit var mPlaylistsRecyclerView: RecyclerView
    private lateinit var playlistAdapter: RecyclerView.Adapter<*>

    private lateinit var mNoPlaylistsTextView: TextView

    private lateinit var mCreatePlaylistFloatingActionButton: FloatingActionButton

    // Model
    private val playlists: MutableList<Playlist> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        SaveSongsUtil.initContext(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlists)

        mPlaylistsRecyclerView = findViewById(R.id.rv_playlists)
        mPlaylistsRecyclerView.setHasFixedSize(true)
        mPlaylistsRecyclerView.layoutManager = LinearLayoutManager(this)

        mNoPlaylistsTextView = findViewById(R.id.tv_no_playlists)

        mCreatePlaylistFloatingActionButton = findViewById(R.id.fab_create_playlist)

        mCreatePlaylistFloatingActionButton.setOnClickListener {
            val dialog = CreatePlaylistDialog(this)
            dialog.show()
        }

        loadRecyclerView()

        // register notification channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel =
                NotificationChannel(NotificationUtil.NOTIFICATION_CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            mChannel.shouldShowLights()
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        val alarmMgr = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val alarmIntent =
            Intent(applicationContext, NotificationReceiver::class.java).let { intent ->
                PendingIntent.getBroadcast(applicationContext, 0, intent, 0)
            }

        alarmMgr.setRepeating(
            AlarmManager.ELAPSED_REALTIME,
            SystemClock.elapsedRealtime() + 60 * 60 * 1000 * 2,
            60 * 60 * 1000 * 2,
            alarmIntent
        )
    }

    override fun onResume() {
        super.onResume()
        updateData(SaveSongsUtil.getSavedPlaylists().playlists)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.mi_search_lyrics -> {
            onSearchRequested()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun loadRecyclerView() {
        playlistAdapter = PlaylistAdapter(this.playlists, this)
        mPlaylistsRecyclerView.adapter = playlistAdapter

        val playlists = SaveSongsUtil.getSavedPlaylists().playlists
        updateData(playlists)
    }

    fun updateData(playlists: MutableList<Playlist>) {
        if (playlists.isEmpty()) {
            mNoPlaylistsTextView.visibility = TextView.VISIBLE
            mPlaylistsRecyclerView.visibility = RecyclerView.GONE
        } else {
            mNoPlaylistsTextView.visibility = TextView.GONE
            mPlaylistsRecyclerView.visibility = RecyclerView.VISIBLE
        }

        this.playlists.clear()
        this.playlists.addAll(playlists)
        playlistAdapter.notifyDataSetChanged()
    }
}
