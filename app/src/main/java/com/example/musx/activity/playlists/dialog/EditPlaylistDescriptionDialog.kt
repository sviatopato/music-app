package com.example.musx.activity.playlists.dialog

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.musx.R
import com.example.musx.activity.playlists.PlaylistsActivity
import com.example.musx.activity.playlists.playlist_content.PlaylistSongsActivity
import com.example.musx.model.Playlist
import com.example.musx.util.SaveSongsUtil
import com.google.android.material.snackbar.Snackbar

class EditPlaylistDescriptionDialog(
    context: Context,
    private val playlist: Playlist
) : Dialog(context),
    View.OnClickListener {

    companion object {
        const val EMPTY_DESCRITPTION = "no description provided"
    }

    private val activity = context as Activity

    private lateinit var mPlaylistDescriptionEditText: EditText
    private lateinit var mEditPlaylistDescriptionButton: Button

    private lateinit var mRoot: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.edit_playlist_description_dialog_layout)

        this.window?.setLayout(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT
        )

        mRoot = findViewById(R.id.cl_edit_playlist_description_dialog_root)

        mPlaylistDescriptionEditText = findViewById(R.id.et_edit_playlist_description)
        mPlaylistDescriptionEditText.setText(playlist.description, TextView.BufferType.EDITABLE)

        mEditPlaylistDescriptionButton = findViewById(R.id.b_edit_playlist_description)
        mEditPlaylistDescriptionButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var newDescription = mPlaylistDescriptionEditText.text.toString()

        if (newDescription.isBlank()) {
            newDescription = EMPTY_DESCRITPTION
        }

        try {
            SaveSongsUtil.editPlaylistDescription(playlist, newDescription)

            playlist.description = newDescription

            activity.let {
                when (it) {
                    is PlaylistSongsActivity -> {
                    }
                    is PlaylistsActivity -> it.updateData(
                        SaveSongsUtil.getSavedPlaylists().playlists
                    )
                    else -> throw IllegalArgumentException("Unexpected error has occurred!")
                }
            }
            this.dismiss()
        } catch (e: IllegalArgumentException) {
            val message = e.message
            if (message != null) {
                Snackbar.make(mRoot, message, Snackbar.LENGTH_LONG)
                    .show()
            } else {
                Snackbar.make(
                        mRoot,
                        "Error has occurred",
                        Snackbar.LENGTH_LONG
                    )
                    .show()
            }
        }
    }
}