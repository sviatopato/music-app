package com.example.musx.activity.playlists.playlist_content.dialog

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.musx.R
import com.example.musx.activity.playlists.playlist_content.PlaylistSongsActivity
import com.example.musx.model.Playlist
import com.example.musx.model.SavedSong
import com.example.musx.util.SaveSongsUtil
import com.google.android.material.snackbar.Snackbar

class EditSongFromPlaylistDialog(
    context: Context,
    private val song: SavedSong,
    private val playlist: Playlist
) : Dialog(context),
    View.OnClickListener {

    companion object {
        const val EMPTY_DESCRITPTION = "no description provided"
    }

    private val activity = context as Activity

    private lateinit var mSongTitleEditText: EditText
    private lateinit var mSongArtistEditText: EditText
    private lateinit var mEditSongButton: Button

    private lateinit var mRoot: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.edit_playlist_song_dialog_layout)

        this.window?.setLayout(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT
        )

        mRoot = findViewById(R.id.cl_edit_song_dialog_root)

        mSongTitleEditText = findViewById(R.id.et_edit_song_title)
        mSongTitleEditText.setText(song.title, TextView.BufferType.EDITABLE)
        mSongArtistEditText = findViewById(R.id.et_edit_song_artist)
        mSongArtistEditText.setText(song.artist, TextView.BufferType.EDITABLE)

        mEditSongButton = findViewById(R.id.b_edit_song)
        mEditSongButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val newTitle = mSongTitleEditText.text.toString()
        val newArtist = mSongArtistEditText.text.toString()

        if (newTitle.isBlank()) {
            showSnackBar("Title can't be blank")
            return
        }

        if (newArtist.isBlank()) {
            showSnackBar("Artist can't be blank")
        }

        try {
            SaveSongsUtil.editSavedSong(playlist, song, newTitle, newArtist)

            song.title = newTitle
            song.artist = newArtist

            activity.let {
                when (it) {
                    is PlaylistSongsActivity -> it.updateData(playlist)
                    else -> throw IllegalArgumentException("Unexpected error has occurred!")
                }
            }
            this.dismiss()
        } catch (e: IllegalArgumentException) {
            e.message?.let { showSnackBar(it) }
        }
    }

    private fun showSnackBar(message: String) {
        Snackbar.make(
                mRoot,
                message,
                Snackbar.LENGTH_LONG
            )
            .show()
    }
}