package com.example.musx.activity.lyrics_search

import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.musx.R
import com.example.musx.content_provider.SearchingHistoryProvider
import com.example.musx.spi.audd.AuddService
import com.example.musx.spi.audd.AuddServiceFactory
import com.example.musx.spi.audd.model.responsemodel.AuddLyricsResponse
import com.example.musx.spi.audd.model.responsemodel.SongData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LyricsSearchableActivity : AppCompatActivity() {

    // Activity members
    // Playlist RecyclerView
    private lateinit var mLyricsRecyclerView: RecyclerView
    private lateinit var lyricsAdapter: RecyclerView.Adapter<*>

    private lateinit var mMessageTextView: TextView
    private lateinit var mProgressBar: ProgressBar

    private lateinit var mRoot: ConstraintLayout

    // Services
    private val lyricsService: AuddService = AuddServiceFactory.create()

    // Model
    private var list = arrayListOf<SongData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lyrics_searchable)

        mLyricsRecyclerView = findViewById(R.id.rv_searched_songs)
        mLyricsRecyclerView.setHasFixedSize(true)
        mLyricsRecyclerView.layoutManager = LinearLayoutManager(this)

        mMessageTextView = findViewById(R.id.tv_no_songs_found)
        mProgressBar = findViewById(R.id.pb_lyrics_search_spinner)

        mRoot = findViewById(R.id.cl_searchable_activity_root)

        lyricsAdapter = LyricsAdapter(list, this, mRoot)
        mLyricsRecyclerView.adapter = lyricsAdapter

        handleIntent(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.mi_search_lyrics -> {
            onSearchRequested()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                SearchRecentSuggestions(
                    this,
                    SearchingHistoryProvider.AUTHORITY,
                    SearchingHistoryProvider.MODE
                )
                    .saveRecentQuery(query, null)
                performSearch(query)
            }
        }
    }

    private fun performSearch(query: String) {
        mProgressBar.visibility = View.VISIBLE
        mLyricsRecyclerView.visibility = View.GONE
        val call: Call<AuddLyricsResponse> = lyricsService.findLyrics(query)
        call.enqueue(object : Callback<AuddLyricsResponse> {
            override fun onFailure(call: Call<AuddLyricsResponse>, t: Throwable) {
                mProgressBar.visibility = View.GONE
                mLyricsRecyclerView.visibility = View.VISIBLE
                updateData(arrayListOf())
                if (!checkInternetConnection()) {
                    showInternetErrorDialog()
                }
                // TODO handle other issues with calling for lyrics
            }

            override fun onResponse(
                call: Call<AuddLyricsResponse>,
                response: Response<AuddLyricsResponse>
            ) {
                mProgressBar.visibility = View.GONE
                mLyricsRecyclerView.visibility = View.VISIBLE
                if (response.isSuccessful) {
                    response.body()?.result?.let { updateData(it) }
                }
            }

        })
    }

    private fun updateData(data: List<SongData>) {
        if (data.isEmpty()) {
            mLyricsRecyclerView.visibility = View.GONE
            mMessageTextView.visibility = View.VISIBLE
        } else {
            mLyricsRecyclerView.visibility = View.VISIBLE
            mMessageTextView.visibility = View.GONE
        }
        list.clear()
        list.addAll(data)
        lyricsAdapter.notifyDataSetChanged()
    }

    private fun showInternetErrorDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("No internet Connection")
        builder.setMessage("Check your Internet connection and try again later")
        builder.setNegativeButton(
            "close"
        ) { dialog, _ ->
            dialog.dismiss()
            finish()
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    private fun checkInternetConnection(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}
