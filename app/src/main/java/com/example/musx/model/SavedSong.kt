package com.example.musx.model

import lombok.NoArgsConstructor
import java.io.Serializable

@NoArgsConstructor
class SavedSong(
    var artist: String,
    var title: String,
    var lyrics: String,
    var attachedSongPath: String = NO_SONG_ATTACHED
) : Serializable {

    companion object {
        const val NO_SONG_ATTACHED = "no_song"
    }

    fun hasSongFromFileAttached(): Boolean {
        return attachedSongPath != NO_SONG_ATTACHED
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SavedSong

        if (artist != other.artist) return false
        if (title != other.title) return false
        if (attachedSongPath != other.attachedSongPath) return false

        return true
    }

    override fun hashCode(): Int {
        var result = artist.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + attachedSongPath.hashCode()
        return result
    }
}