package com.example.musx.model

import java.io.Serializable

class Playlist(var name: String, var description: String) : Serializable {

    constructor(name: String, description: String, songs: MutableList<SavedSong>) : this(
        name,
        description
    ) {
        this.songs = songs
    }

    var songs: MutableList<SavedSong> = arrayListOf()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Playlist

        if (name != other.name) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + description.hashCode()
        return result
    }


}