package com.example.musx.model

import java.io.Serializable

data class SavedPlaylists(val playlists: MutableList<Playlist>) : Serializable