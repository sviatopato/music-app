package com.example.musx

import android.app.Application
import android.content.Intent
import android.util.Log
import kotlin.system.exitProcess

class MusXApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Thread.setDefaultUncaughtExceptionHandler { _, paramThrowable ->
            paramThrowable.printStackTrace()
            val stackTrace = Log.getStackTraceString(paramThrowable)
            val emailIntent = Intent(Intent.ACTION_SEND)

            emailIntent.type = "message/rfc2"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("svyatopato@gmail.com"))

            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "MusX crash report")
            emailIntent.putExtra(Intent.EXTRA_TEXT, stackTrace)
            emailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            val chooserIntent = Intent(Intent.ACTION_CHOOSER)
            chooserIntent.putExtra(Intent.EXTRA_INTENT, emailIntent)
            chooserIntent.putExtra(
                Intent.EXTRA_TITLE,
                "Error occurred. Choose email app to report.."
            )
            chooserIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(chooserIntent)

            exitProcess(2)
        }
    }
}