package com.example.musx.content_provider

import android.content.SearchRecentSuggestionsProvider

class SearchingHistoryProvider : SearchRecentSuggestionsProvider() {
    init {
        setupSuggestions(AUTHORITY, MODE)
    }

    companion object {
        const val AUTHORITY = "com.example.musx.content_provider.SearchingHistoryProvider"
        const val MODE: Int = DATABASE_MODE_QUERIES
    }
}