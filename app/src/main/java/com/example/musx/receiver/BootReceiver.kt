package com.example.musx.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.example.musx.service.NotificationService

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, recievedIntent: Intent) {
        if (recievedIntent.action == Intent.ACTION_BOOT_COMPLETED) {
            val i = Intent(context, NotificationService::class.java)
            context.startService(i)
        }
    }
}
