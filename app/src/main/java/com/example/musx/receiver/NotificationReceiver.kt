package com.example.musx.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.musx.util.NotificationUtil

class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        NotificationUtil.showNotification(context)
    }
}
