package com.example.musx.util

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.musx.R
import com.example.musx.activity.playlists.PlaylistsActivity

class NotificationUtil {

    companion object {
        const val NOTIFICATION_CHANNEL_ID =
            "com.example.musx.activity.playlists.NOTIFICATION_CHANNEL_ID"

        private var notificationId = 0

        fun showNotification(context: Context) {
            val intent = Intent(context, PlaylistsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

            val builder = NotificationCompat.Builder(
                    context,
                    NOTIFICATION_CHANNEL_ID
                )
                .setSmallIcon(R.drawable.song_icon_96)
                .setContentTitle("MusX")
                .setContentText("Attach songs and sing!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

            with(NotificationManagerCompat.from(context)) {
                notify(notificationId++, builder.build())
            }
        }
    }
}