package com.example.musx.util

import android.app.Activity
import android.content.Context
import android.util.Log
import com.example.musx.model.Playlist
import com.example.musx.model.SavedPlaylists
import com.example.musx.model.SavedSong
import java.io.*

class SaveSongsUtil {
    companion object {
        private const val FILE_NAME = "lyrics_and_songs.storage"

        private var ctx: Context = Activity()

        fun initContext(ctx: Context) {
            this.ctx = ctx
        }

        fun getSavedPlaylists(): SavedPlaylists {
            val file = File(FILE_NAME)

            Log.d(
                SaveSongsUtil::class.java.simpleName,
                "File info is get from file $file"
            )

            try {
                val fIn: FileInputStream = ctx.openFileInput(FILE_NAME)
                val oIn = ObjectInputStream(fIn)

                val possiblePlaylist: Any = oIn.readObject()

                oIn.close()

                if (possiblePlaylist is SavedPlaylists) {
                    Log.d(
                        SaveSongsUtil::class.java.simpleName,
                        "Read data from file: $possiblePlaylist"
                    )
                    return possiblePlaylist
                }
            } catch (e: FileNotFoundException) {
                val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
                val oOut = ObjectOutputStream(fOut)

                oOut.writeObject(SavedPlaylists(arrayListOf()))

                oOut.close()
            }

            return SavedPlaylists(ArrayList())
        }

        fun addNewPlaylist(playlist: Playlist) {
            val savedPlaylists = getSavedPlaylists()

            for (i in 0 until savedPlaylists.playlists.size) {
                if (savedPlaylists.playlists[i].name == playlist.name) {
                    throw IllegalArgumentException("Playlist with name ${playlist.name} already exists")
                }
            }

            savedPlaylists.playlists.add(playlist)

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()
        }


        fun editPlaylistName(
            playlist: Playlist,
            newPlaylistName: String
        ): SavedPlaylists {
            val savedPlaylists = getSavedPlaylists()

            for (i in 0 until savedPlaylists.playlists.size) {
                if (savedPlaylists.playlists[i].name == newPlaylistName) {
                    throw IllegalArgumentException("Playlist with name ${playlist.name} already exists")
                }
            }

            val index = savedPlaylists.playlists.indexOf(playlist)
            savedPlaylists.playlists[index].name = newPlaylistName

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()

            return savedPlaylists
        }

        fun editPlaylistDescription(
            playlist: Playlist,
            newPlaylistDescription: String
        ): SavedPlaylists {
            val savedPlaylists = getSavedPlaylists()

            val index = savedPlaylists.playlists.indexOf(playlist)
            savedPlaylists.playlists[index].description = newPlaylistDescription

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()

            return savedPlaylists
        }

        fun removePlaylist(playlist: Playlist): SavedPlaylists {
            val savedPlaylists = getSavedPlaylists()

            if (!savedPlaylists.playlists.remove(playlist)) {
                Log.e(
                    SaveSongsUtil::class.java.simpleName,
                    "Failed to remove playlist with name ${playlist.name}"
                )
            }

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()

            return savedPlaylists
        }

        fun addSongToPlaylist(playlist: Playlist, savedSong: SavedSong) {
            val savedPlaylists = getSavedPlaylists()

            val index = savedPlaylists.playlists.indexOf(playlist)

            for (i in 0 until savedPlaylists.playlists[index].songs.size) {
                if (savedPlaylists.playlists[index].songs[i].title == savedSong.title
                    && savedPlaylists.playlists[index].songs[i].artist == savedSong.artist
                ) {
                    throw IllegalArgumentException("Song with title ${savedSong.title} by ${savedSong.artist} already exists in ${playlist.name} playlist")
                }
            }

            savedPlaylists.playlists[index].songs.add(savedSong)

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()
        }

        fun editSavedSong(
            playlist: Playlist,
            savedSong: SavedSong,
            newTitle: String,
            newArtist: String
        ): Playlist {
            val savedPlaylists = getSavedPlaylists()

            val playlistIndex = savedPlaylists.playlists.indexOf(playlist)
            val songIndex = savedPlaylists.playlists[playlistIndex].songs.indexOf(savedSong)

            for (i in 0 until savedPlaylists.playlists[playlistIndex].songs.size) {
                if (savedPlaylists.playlists[playlistIndex].songs[i].title == newTitle
                    && savedPlaylists.playlists[playlistIndex].songs[i].artist == newArtist
                ) {
                    throw IllegalArgumentException("Song with title ${savedSong.title} of artist ${savedSong.artist} already exists in this playlist")
                }
            }

            savedPlaylists.playlists[playlistIndex].songs[songIndex].artist = newArtist
            savedPlaylists.playlists[playlistIndex].songs[songIndex].title = newTitle

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()

            return savedPlaylists.playlists[playlistIndex]
        }

        fun attachSongToSavedSong(
            playlist: Playlist,
            savedSong: SavedSong,
            pathToSong: String
        ): Playlist {
            val savedPlaylists = getSavedPlaylists()

            val playlistIndex = savedPlaylists.playlists.indexOf(playlist)
            val songIndex = savedPlaylists.playlists[playlistIndex].songs.indexOf(savedSong)
            savedPlaylists.playlists[playlistIndex].songs[songIndex].attachedSongPath = pathToSong

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()

            return savedPlaylists.playlists[playlistIndex]
        }

        fun removeSavedSongFromPlaylist(
            playlist: Playlist,
            savedSong: SavedSong
        ): Playlist {
            val savedPlaylists = getSavedPlaylists()

            val playlistIndex = savedPlaylists.playlists.indexOf(playlist)
            savedPlaylists.playlists[playlistIndex].songs.remove(savedSong)

            val fOut: FileOutputStream = ctx.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            val oOut = ObjectOutputStream(fOut)

            oOut.writeObject(savedPlaylists)

            oOut.close()

            return savedPlaylists.playlists[playlistIndex]
        }
    }
}