package com.example.musx.util

import android.content.Context
import android.net.Uri
import com.example.musx.model.SavedSong

class FileUtils {

    companion object {
        fun getPath(context: Context, uri: Uri): String {
            if ("content".equals(uri.scheme, ignoreCase = true)) {
                val projection = arrayOf("_data")
                val cursor = context.contentResolver.query(uri, projection, null, null, null)
                if (cursor != null) {
                    val columnIndex: Int? = cursor.getColumnIndexOrThrow("_data")
                    if (cursor.moveToFirst()) {
                        val path = columnIndex?.let { cursor.getString(it) }
                        cursor.close()
                        return path.toString()
                    }
                }
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path.toString()
            }
            return SavedSong.NO_SONG_ATTACHED
        }
    }
}