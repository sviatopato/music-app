package com.example.musx.spi.audd.model.responsemodel

import java.io.Serializable

data class SongData(
    var title: String, var artist: String, var lyrics: String
) : Serializable