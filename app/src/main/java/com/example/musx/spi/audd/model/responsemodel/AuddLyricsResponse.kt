package com.example.musx.spi.audd.model.responsemodel

import lombok.Data
import lombok.NoArgsConstructor

@Data
@NoArgsConstructor
data class AuddLyricsResponse(val status: String, val result: List<SongData>)