package com.example.musx.spi.audd

import com.example.musx.spi.audd.model.responsemodel.AuddLyricsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface AuddService {

    companion object {
        private const val API_KEY: String = "cc9f3d7c051ff2cd3f2f9c98949d833f"
    }

    @GET("/findLyrics/?api_token=$API_KEY")
    fun findLyrics(@Query("q") lyricsOrSongName: String): Call<AuddLyricsResponse>
}