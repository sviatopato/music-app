package com.example.musx.spi.audd;

import com.example.musx.spi.audd.model.responsemodel.AuddLyricsResponse;

import org.junit.Test;

import java.util.Objects;

import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Response;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class AuddServiceTest {

    @SneakyThrows
    @Test
    public void test() {
        AuddService service = AuddServiceFactory.INSTANCE.create();
        Call<AuddLyricsResponse> call = service.findLyrics("Eminem");
        Response<AuddLyricsResponse> response = call.execute();
        assertEquals(200, response.code());
        assertTrue(response.isSuccessful());
        assertTrue(call.isExecuted());
        assertFalse(call.isCanceled());
        assertNotNull(response.body());
        assertTrue(Objects.requireNonNull(response.body().getResult()).size() > 0);

        call.cancel();
        assertTrue(call.isCanceled());
    }
}
